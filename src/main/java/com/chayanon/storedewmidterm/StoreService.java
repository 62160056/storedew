/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chayanon.storedewmidterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W I N 1 0
 */
public class StoreService {
    private static ArrayList<StoreDew> storeList =null;
    
    static{
        
    }
    
    public static boolean addStore(StoreDew store){
        storeList.add(store) ;   
        save();
        return true;
    }
    public static boolean delStore(StoreDew store) {
        storeList.remove(store); 
        save();
        return true;
    }
    public static boolean delStore(int index) {
        storeList.remove(index);   
        save();
        return true;
    }
    public static boolean updateStore(int index, StoreDew store) {
        storeList.set(index, store); 
        save();
        return true;
    }
     public static ArrayList<StoreDew> getStores() {
        return storeList;
    }

    public static StoreDew getStore(int index) {
        return storeList.get(index);
    }
    public static boolean clear(){
        storeList.removeAll(storeList);
        save();
        return true;
        
    }
    public static int size(){
        return storeList.size();
    }
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {

            file = new File("dew.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(storeList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {

            file = new File("dew.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            storeList = (ArrayList<StoreDew>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
